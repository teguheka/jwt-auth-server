package id.teguh.training.entity;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}