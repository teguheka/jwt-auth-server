package id.teguh.training.controller;

import id.teguh.training.dto.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class PersonController {
    private static final List<Person> persons;

    static {
        persons = new ArrayList<>();
        persons.add(new Person("Hello", "World"));
        persons.add(new Person("Foo", "Bar"));
    }

    @RequestMapping(path = "/persons", method = RequestMethod.GET)
    public static List<Person> getPersons() {
        return persons;
    }

    @RequestMapping(path = "/persons/{name}", method = RequestMethod.GET)
    public static Person getPerson(@PathVariable("name") String name) {
        return persons.stream().filter(person -> name.equalsIgnoreCase(person.getName())).findAny().orElse(null);
    }

    @Autowired private PasswordEncoder passwordEncoder;

    @RequestMapping(path = "/password/encode/{password}", method = RequestMethod.GET)
    public String hashPassword(@PathVariable String password) {
        return passwordEncoder.encode(password);
    }
}


